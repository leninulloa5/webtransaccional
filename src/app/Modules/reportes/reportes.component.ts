import { Component, OnInit } from '@angular/core';
import { LoginserviceService } from '../Services/loginservice.service';
import { CookieService } from 'ngx-cookie-service';
import { NgxDateRangeModule } from 'ngx-daterange';
import { ReporteServiceService } from '../Services/reporte-service.service';
import Swal from 'sweetalert2';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit {
 dataTable:any=[];
  username: any;
  dateRange: any;
  listAsesores: any = [];
  dateStart: any;
  dateEnd: any;
  options = { labelText: 'Rango de Fechas', format: 'YYYY-MM-DD' }
  data: any = { sumadepo: 0, sumacobros: 0, depo: [], cobros: [] };
  userId!: string;
  type: any;
  sumadepo: any;
  sumacobros: any;
  date: any;
  formulario = new FormGroup({ 'dateRange': new FormControl('fecha') })
  selectedAsesor: any;
  //
  user: any
  constructor(
    private _serviceReporte: ReporteServiceService,
    private _cookies: CookieService,
    public _serviceLogin: LoginserviceService
  ) {

  }

  ngOnInit(): void {

    this.username = this._cookies.get('user')
    if(this.username){
      this.username=JSON.parse(this.username)
    }else{
      location.href="/login"
    }
    this.userId = this._cookies.get('userId')
    this._serviceReporte.allUsers().subscribe((resp) => {
      console.log(resp)
      this.listAsesores = resp.users
    })
  }
  pickDate1(event: any) {
    console.log("datetimerangueeee")

    this.dateStart = event.target.value
    console.log(this.dateStart);


  }
  pickDate2(event: any) {
    console.log("datetimerangueeee 2")
    if (this.dateStart < event.target.value) {
      console.log("valido")
      this.dateEnd = event.target.value
      console.log(this.dateEnd)

    }

    else {
      Swal.fire(
        'Error',
        'En el rango de fechas',

      )
      window.location.reload();

    }


  }
  findByRange() {

    console.log(this.formulario.value)
    var aux = this.formulario.value.dateRange.split(' - ')
    console.log(aux)
    if (aux.length > 1) {
      this.dateStart = aux[0]
      this.dateEnd = aux[1]
      console.log('type', this.type)
      console.log('asesor', this.selectedAsesor)
      console.log('datestar', this.dateStart)
      console.log('dateend', this.dateEnd)
      this._serviceReporte.DatabyRange(this.type, this.selectedAsesor, this.dateStart, this.dateEnd).subscribe((resp) => {
        console.log(resp)
        this.data = resp
        this.dataTable=[...resp.cobros,... resp.depositos]
        console.log(this.dataTable)
      })

    } else {
      Swal.fire(
        'Error',
        'Ingrese una fecha correcta',

      )
    }

  }
localDate(date:any){
var aux=new Date(date)
return aux.toLocaleString()

}
logOut(){
  this._cookies.deleteAll();
  location.href="/login"
}
}
