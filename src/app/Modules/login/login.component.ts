import { Component, OnInit } from '@angular/core';
import { LoginserviceService } from '../Services/loginservice.service';
import swal from 'sweetalert2';
import { CookieService} from 'ngx-cookie-service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: any = "";;
  password: any = "";;

  constructor(
    private serviceLogin: LoginserviceService, 
    private _cookies:CookieService
    ) { }

  ngOnInit(): void {
  }
  ingresar(name: any, password: any) {
    
      // this.serviceLogin.setUsername=resp
      // this.serviceLogin.getUsername()
      if (name == '' || password == '') {
        swal.fire({
          title: 'Campos vacíos',
          text: "",
          icon: 'error'
        })
      } else {
        this.serviceLogin.login(name, password).subscribe((resp) => {
           
        if (resp.status == 'ok') {
          //seteo las variables
          console.log(resp)
          this._cookies.set('user', JSON.stringify(resp.user))
          this._cookies.set('userId',JSON.stringify(resp.user))
          console.log('****')
          console.log(this._cookies.get('user'))
          
          console.log(this._cookies.get('userId'))
          

//continuoo con el procesp
          swal.fire('Logeado con éxito', '', "success")
            .then(() => {
            location.href = ('reportes')
            })

        } else {
          swal.fire({
            title: 'Error al conectar',
            text: "",
            icon: 'error'
          })
        }
      })
    }
  }
}
