import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { GLOBAL } from './Global';
import { CookieService } from 'ngx-cookie-service'



@Injectable({
  providedIn: 'root'
})
export class LoginserviceService {
  url: string = "";

  public email: any = ""
  public phone: any = ""
  constructor(
    private _http: HttpClient,
    private _cookies: CookieService

  ) {
    this.url = GLOBAL.url;
  }

  login(name: any, password: any): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this._http.post<any>(`${this.url}recaudacion-web/login-recaudacionweb`, { name, password }, { headers });
  }
  //setters
  setUsername(username: any) {
    this._cookies.set('username', username);

  }
  setUserid(userId: any) {
    this._cookies.set('userId', userId)

  }

  //getters

  getUsername() {
    return this._cookies.get('username')
  }
  getUSerID() {
    return this._cookies.get('userId')
  }




}
