import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GLOBAL } from './Global';
@Injectable({
  providedIn: 'root'
})
export class ReporteServiceService {
  url: string;
   DatabyRange(type:any,idAsesor:any,dateStart:any,dateEnd:any): Observable<any>{
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this._http.post<any>(`${this.url}recaudacion-web/find-moved-recaudacionweb`, {type,idAsesor,dateStart,dateEnd}, {headers});
  }
  allUsers():Observable<any>{
   
    return this._http.get<any>(`${this.url}recaudacion-web/all-user-recaudacionweb`)
  }
  
  constructor(private _http:HttpClient) { 
    this.url = GLOBAL.url;

  }
}
