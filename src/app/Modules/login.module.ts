import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login/login.component';
import { ReportesComponent } from './reportes/reportes.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { CookieService } from 'ngx-cookie-service';
import { NgxDateRangeModule } from 'ngx-daterange';
@NgModule({
  declarations: [
    LoginComponent,
    ReportesComponent,
    SidebarComponent,
  ],
  imports: [
    CommonModule,
    NgxDateRangeModule,
    LoginRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ], 
  providers: [],
})
export class LoginModule { }
