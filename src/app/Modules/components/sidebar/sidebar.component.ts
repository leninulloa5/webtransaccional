import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor(
    private _cookies:CookieService
  ) { }

  ngOnInit(): void {
  }
  logOut(){
    this._cookies.deleteAll();
    location.href="/login"
  }
}
