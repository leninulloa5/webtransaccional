import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ReportesComponent } from './reportes/reportes.component';

const routes: Routes = [

  {path: "", redirectTo: "login", pathMatch: "full"},
  {path: "", component: LoginComponent}, 
  { path: 'login', component: LoginComponent },
  { path: 'reportes', component: ReportesComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
